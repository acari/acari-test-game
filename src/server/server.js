import config   from './config'
import acari    from 'acari-server'
import map      from '../shared/maps/office'
import Dog      from '../shared/dog.js'
import EasyStar from 'easystarjs'

var players = [
  new Dog({id: 0, socket: null, bot: true, x: 10, y: 10}),
  new Dog({id: 1, socket: null, bot: true, x: 11, y: 10}),
  new Dog({id: 2, socket: null, bot: true, x: 12, y: 10}),
  new Dog({id: 3, socket: null, bot: true, x: 13, y: 10}),
  new Dog({id: 4, socket: null, bot: true, x: 14, y: 10}),
  new Dog({id: 5, socket: null, bot: true, x: 15, y: 10}),
  new Dog({id: 6, socket: null, bot: true, x: 10, y: 11}),
  new Dog({id: 7, socket: null, bot: true, x: 11, y: 11}),
  new Dog({id: 8, socket: null, bot: true, x: 12, y: 11}),
  new Dog({id: 9, socket: null, bot: true, x: 13, y: 11}),
  new Dog({id: 10, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 11, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 12, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 13, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 14, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 15, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 16, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 17, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 18, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 19, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 20, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 21, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 22, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 23, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 24, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 25, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 26, socket: null, bot: true, x: 14, y: 11}),
  new Dog({id: 27, socket: null, bot: true, x: 15, y: 11})
]

var nextPlayer = players.length
var waitingForPath = []

var server = acari({
  config: config,
  clientConnectedFn: (socketid) => {
    console.log('GAME: client ' + socketid + ' connected')
    const newplayer = new Dog({ id: nextPlayer, socket: socketid, bot: false, x: 15, y: 15 })
    nextPlayer++
    players.push(newplayer)
  },
  clientDisconnectedFn: () => {
    console.log('GAME: client disconnected')
  },
  clientEventFn: (client, eventData) => {
    // console.log('GAME: event from client', client, eventData)
    if (eventData === 'ping') {
      receivedPing()
    }
    if (eventData === 'pong') {
      receivedPong(client)
    }
  },
  clientActionFn: (client, actionData) => {
    console.log('GAME: action from client', client, actionData)
    if (actionData.type === 'click') {
      console.log('--> IT´S A CLICK!')
      for (const player of players) {
        console.log('Checking if', player.socket, 'equals', client)
        if (player.socket === client) {
          console.log('SHOULD CREATE PATH!')
          createPathForPlayer(player, actionData.x, actionData.y)
        }
      }
    }
  },
  updateTickFn: () => {
    const ticktime = new Date()
    console.log('GAME: tick -> ' + ticktime.getHours() + ':' + ticktime.getMinutes() + ':' + ticktime.getSeconds())
    updateGameState()
  }
})

initializeEasyStar()

function initializeEasyStar() {
  console.log('INITIALIZING EASYSTAR')
  server.pathfinderMap = createPathfinderMap()
  /* eslint-disable new-cap */
  server.easystar = new EasyStar.js()
  /* eslint-enable new-cap */
  server.easystar.setGrid(server.pathfinderMap)
  server.easystar.setAcceptableTiles([0])
  server.easystar.enableDiagonals()
  server.easystar.disableCornerCutting()
  console.log('ALL DONE INITIALIZING EASYSTAR')
}

function receivedPing() {
  console.log('GAME: Server was pinged')
  server.emitMessage('pong')
}

function receivedPong(client) {
  console.log('GAME: Server received pong response')
}

function updateGameState() {
  for (const player of players) {
    if (player.bot) {
      if (!player.path || Object.keys(player.path).length === 0) {
        console.log('Bot has no path, should generate!')
        var targetX = getRandomInt(0, map.width - 1)
        var targetY = getRandomInt(0, map.height - 1)

        createPathForPlayer(player, targetX, targetY)
      }
    }

    if (player.path && player.path.length > 0) {
      const movecoords = player.path.shift()
      player.x = movecoords.x
      player.y = movecoords.y

      const index = waitingForPath.indexOf(player.id)
      if (index !== -1) {
        waitingForPath.splice(index, 1)
      }
    }
  }
  server.easystar.calculate()
  server.emitMessage('stateupdate', players)
}

function createPathForPlayer(player, targetX, targetY) {
  const alreadyWaitingForPath = waitingForPath.indexOf(player.id) !== -1
  const targetTile = server.pathfinderMap[targetY][targetX]
  console.log('Trying to create path for: ', player, targetX, targetY)
  if (targetTile === 0 && !alreadyWaitingForPath) {
    const startX = player.x
    const startY = player.y
    // const startTile = server.pathfinderMap[startY][startX]
    console.log('LEGAL PATH --> CREATING PATH!')
    waitingForPath.push(player.id)
    server.easystar.findPath(startX, startY, targetX, targetY, player.setPath.bind(player))
  } else if (alreadyWaitingForPath) {
    console.log('ALREADY WAITING FOR PATH --> NOT CREATING')
  } else {
    console.log('ILLEGAL PATH --> NOT CREATING!')
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

// function isCellLegal(x, y) {
//   var collisionLayer = map.layers.filter(layer => layer.name === 'collision')[0]
//   return collisionLayer.data[ y * collisionLayer.width + x] === 0
// }

function createPathfinderMap() {
  const collisionLayer = map.layers.filter(layer => layer.name === 'collision')[0]
  const pathfinderMap = []

  console.log('parsing the map: ', map.width, map.height)

  while (collisionLayer.data.length) pathfinderMap.push(collisionLayer.data.splice(0, map.width))

  pathfinderMap.forEach(function(row) {
    let rowdata = '' + row
    rowdata = rowdata.split('498').join('1').split(',').join('')
    console.log(rowdata)
  })

  console.log('All good!')
  return pathfinderMap
}

console.log('TEST GAME SERVER')
