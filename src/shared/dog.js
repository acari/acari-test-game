export default class Dog {

  constructor(object) {
    this.id = object.id
    this.bot = object.bot
    this.socket = object.socket
    this.x = object.x
    this.y = object.y
    this.path = []
  }

  update() {
    console.log('updating dog')
  }

  setPath(path) {
    // console.log('Path received!', path)
    this.path = path
  }
}
