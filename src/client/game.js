import 'pixi'
import 'p2'
import Phaser from 'phaser'

import PreloadState from './states/PreloadState'
import GameState from './states/GameState'

class Game extends Phaser.Game {
  constructor() {
    const width = document.documentElement.clientWidth > 1024 ? 1024 : document.documentElement.clientWidth
    const height = document.documentElement.clientHeight > 600 ? 600 : document.documentElement.clientHeight

    super(width, height, Phaser.AUTO, 'content', null)
    this.state.add('Preload', PreloadState, false)
    this.state.add('Game', GameState, false)

    this.state.start('Preload')
  }
}

window.game = new Game()
