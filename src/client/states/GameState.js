import Phaser from 'phaser'

export default class extends Phaser.State {

  preload() {
    this.game.stage.disableVisibilityChange = true
    this.game.clickaction = false
  }

  setupSocketEvents() {
    this.game.socket.on('pong', this.pingReceived.bind(this))
    this.game.socket.on('stateupdate', this.checkDogs.bind(this))
  }

  pingReceived(msg) {
    this.pingState.pingReceived = this.game.time.now
    this.pingState.pingTime = this.pingState.pingReceived - this.pingState.pingSent
    this.pingText.setText('Ping to server: ' + this.pingState.pingTime)
  }

  checkDogs(msg) {
    if (this.currentState === 'NOT_READY') {
      this.initDogs(msg)
    } else {
      this.updateDogs(msg)
    }
  }

  create() {
    this.pingState = { waiting: true, pingSent: 0, pingReceived: 0, pingCooldown: 3000, pingTime: 0 }
    this.pingCooldown = 2000

    this.bgLayer = this.game.add.group()

    this.fgLayer = this.game.add.group()
    this.itemLayer = this.game.add.group()
    this.fgLayer.add(this.itemLayer)

    this.dogSprites = this.game.add.group()
    this.fgLayer.add(this.dogSprites)

    this.dogHighlight = {}

    this.dogs = {}
    this.setupSocketEvents()

    var map = this.game.add.tilemap('office')

    map.tilesets.forEach(function(tileset) {
      map.addTilesetImage(tileset.name)
    })

    this.pingText = this.game.add.text(10, 600 - 32, 'Ping to server: waiting for first', {
      font: '16px Arial',
      fill: '#ffffff',
      align: 'center'
    })

    this.pingText.anchor.setTo(0, 1)
    this.pingText.fixedToCamera = true

    this.floorLayer = map.createLayer('floor')
    this.wallLayer = map.createLayer('walls')
    this.furnitureLayer = map.createLayer('furniture')

    this.bgLayer.add(this.floorLayer)
    this.bgLayer.add(this.wallLayer)
    this.bgLayer.add(this.furnitureLayer)
    this.floorLayer.resizeWorld()

    // var collisionLayer = map.createLayer('collision')
    // this.bgLayer.add(collisionLayer)
  }

  update() {
    this.pingCooldown -= this.game.time.elapsedMS
    if (this.pingCooldown < 0) {
      this.game.socket.emit('event', 'ping')
      this.pingState.pingSent = this.game.time.now
      this.pingCooldown = 2000
    }

    if (this.playerDog && this.dogHighlight) {
      this.dogHighlight.x = this.playerDog.x - 1
      this.dogHighlight.y = this.playerDog.y + 10
    }

    const pointer = this.game.input.activePointer
    if (!this.game.clickaction && pointer.leftButton.isDown) {
      const tileX = pointer.x + this.game.camera.x
      const tileY = pointer.y + this.game.camera.y
      const tileCoordsX = Math.floor(tileX / 16)
      const tileCoordsY = Math.floor(tileY / 16)

      this.game.clickaction = true
      const click = {type: 'click', x: tileCoordsX, y: tileCoordsY}
      this.game.socket.emit('action', click)
      console.log('Click!', click)
    } else if (this.game.clickaction && pointer.leftButton.isUp) {
      this.game.clickaction = false
    }
  }

  initDogs(msg) {
    this.dogs = msg
    console.log('Initializing dogs:', this.dogs)
    this.dogSprites = this.game.add.group()

    for (const dog of this.dogs) {
      this.createDog(dog.x, dog.y, dog.id, dog.socket)
    }

    this.currentState = 'READY'
  }

  updateDogs(msg) {
    this.dogs = msg

    for (const dog of this.dogs) {
      const results = this.dogSprites.children.filter(d => d.serverId === dog.id)
      if (results.length > 0) {
        const currentDog = results[0]
        this.game.add.tween(currentDog).to({x: dog.x * 16, y: dog.y * 16}, 250, Phaser.Easing.Linear.None, true)
      } else {
        this.createDog(dog.x, dog.y, dog.id, dog.socket)
      }
    }

    const removableDogs = this.dogSprites.children.filter(d => {
      return this.dogs.filter(dog => d.serverId === dog.id).length === 0
    })

    for (const removableDog of removableDogs) {
      removableDog.destroy()
    }
  }

  createDog(x, y, id, socketid) {
    const dogSprite = this.game.add.sprite(x * 16, y * 16, 'dog')
    dogSprite.animations.add('pant')
    dogSprite.animations.play('pant', 10, true)
    dogSprite.serverId = id
    this.dogSprites.add(dogSprite)

    if (socketid === this.game.socket.id) {
      console.log('This is the player dog!')
      this.playerDog = dogSprite
      this.dogHighlight = this.game.add.sprite(dogSprite.x - 1, dogSprite.y + 10, 'highlight')
      this.itemLayer.add(this.dogHighlight)
      this.game.camera.follow(this.playerDog)
    }

    console.log('Dog id: ', id, ' socket id: ', socketid || 'bot')
  }

  render() {
    // this.game.debug.cameraInfo(this.game.camera, 32, 32)
  }

}
