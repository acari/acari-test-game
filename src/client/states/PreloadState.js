import Phaser from 'phaser'

export default class extends Phaser.State {

  preload() {
    console.log('Preload state!')
    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
    console.log('Setting up socket')
    /* eslint-disable no-undef */
    this.game.socket = io('http://localhost:3000')
    /* eslint-enable no-undef */
    console.log('Socket set', this.game.socket)
    this.game.load.spritesheet('dog', 'assets/images/corgi-animated-scaled.png', 16, 16)
    this.game.load.image('bark', 'assets/images/bark.png')
    this.game.load.image('barkbutton', 'assets/images/barkbutton.png')
    this.game.load.image('roguelikeSheet_transparent', 'assets/maps/roguelikeSheet_transparent.png')
    this.game.load.image('roguelikeIndoor_transparent', 'assets/maps/roguelikeIndoor_transparent.png')
    this.game.load.image('duck', 'assets/images/duck.png')
    this.game.load.tilemap('office', 'assets/maps/office.json', null, Phaser.Tilemap.TILED_JSON)
    this.game.load.image('highlight', 'assets/images/highlight.png')
  }

  update() {
    console.log('Moving to game state!')
    this.state.start('Game')
  }
}
