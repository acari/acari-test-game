# ACARI test game


# Developing test game in local environment

You need to change from `package.json` file acari-server
location to point your local copy of acari-server.

Remember to build acari-server after you have made changes
to it. Then you should install again acari-server using command
`npm install ../acari-server`


# Running test game

`npm run dev`
