FROM node:6.7.0

EXPOSE 8000

# Fixes Issue 9863
RUN cd $(npm root -g)/npm && npm install fs-extra && sed -i -e s/graceful-fs/fs-extra/ -e s/fs\.rename/fs.move/ ./lib/utils/rename.js && npm install -g npm

ADD package.json /acari-test-game/package.json
RUN cd /acari-test-game/ && npm install 

ADD . /acari-test-game
WORKDIR /acari-test-game
CMD node ./src/app.js
